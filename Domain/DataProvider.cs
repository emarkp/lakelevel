﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LakeLevel.Domain
{
    public class DataProvider
    {
        public enum Lake { ORO, SHASTA };

        public static List<LevelValue> LoadLake(Lake lake)
        {
            var allLakeValues = new List<LevelValue>();
            var fileName = lake == Lake.ORO ? "Oro.txt" : "Sha.txt";
            var path = $"..\\..\\..\\Resources\\{fileName}";
            foreach (var line in File.ReadLines(path))
            {
                var pieces = line.Split('\t');
                var d = DateOnly.Parse(pieces[0]);
                var l = decimal.Parse(pieces[1]);
                var single = new LevelValue
                {
                    LevelDate = d,
                    Level = l
                };
                allLakeValues.Add(single);
            }
            return allLakeValues;
        }
    }
}
