﻿using System;

namespace LakeLevel.Domain
{
    public  class LevelValue
    {
        public DateOnly LevelDate { get; set; }
        public decimal Level { get; set; }
    }
}
