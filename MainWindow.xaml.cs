﻿using LakeLevel.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace LakeLevel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private LakeViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            viewModel = new LakeViewModel();
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var sItem = ((ComboBox)sender).SelectedIndex;
            viewModel.SelectedLakeComboIndex(sItem);

        }
    }
}
