﻿using LakeLevel.Domain;
using System;
using System.Collections.Generic;
using static LakeLevel.Domain.DataProvider;

namespace LakeLevel.ViewModel
{
    public class LakeViewModel : ViewModelBase
    {
        private Lake? selectedLake;

        public string? Name
        {
            get
            {
                if (SelectedLake is null)
                {
                    return null;
                }
                else if (SelectedLake == Lake.ORO)
                {
                    return "Oroville";
                }
                else return "Shasta";
            }
        }

        public List<LevelValue>? LevelValues { get; set; }

        public Lake? SelectedLake
        {
            get
            {
                return selectedLake;
            }
            set
            {
                selectedLake = value;
                if (value is not null)
                {
                    LevelValues = DataProvider.LoadLake(value.Value);
                }
                else
                {
                    LevelValues = null;
                }

                RaisePropertyChanged(nameof(IsLakeSelected));

            }
        }

        public bool IsLakeSelected() => SelectedLake is not null;

        internal void SelectedLakeComboIndex(int comboSelectionIndex)
        {
            Domain.DataProvider.Lake? lake = null;
            if (comboSelectionIndex == 1)
            {
                lake = Domain.DataProvider.Lake.SHASTA;
            }
            else if (comboSelectionIndex == 0)
            {
                lake = Domain.DataProvider.Lake.ORO;
            }
            SelectedLake = lake;
        }

        public LakeViewModel()
        {
            SelectedLake = null;
        }
    }
}
